package com.xdl.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.xdl.dao.NoteDao;
import com.xdl.entity.Dept;
import com.xdl.entity.Note;

/**
 * 列表展示
 * @author likang
 * @date   2018-1-8 下午2:43:23
 */
@Controller//id-----listAction
@Scope(value = "prototype")
public class ListAction {

//	public List<Dept> listDept;
	public List<Note> listNote;
	
	@Resource
	private NoteDao noteDao;
	
	public String execute(){
//		listDept = new ArrayList<Dept>();
//		for (int i = 0; i < 10; i++) {
//			Dept dept = new Dept();
//			dept.setId(Long.valueOf(i));
//			dept.setDeptName("java13_"+i);
//			dept.setDeptNote("BeiJing"+i);
//			listDept.add(dept);
//		}
		
		listNote = noteDao.queryNoteByuserId(1);
		return "success";
	}

	public List<Note> getListNote() {
		return listNote;
	}

	public void setListNote(List<Note> listNote) {
		this.listNote = listNote;
	}

//	public List<Dept> getListDept() {
//		return listDept;
//	}
//
//	public void setListDept(List<Dept> listDept) {
//		this.listDept = listDept;
//	}
}
