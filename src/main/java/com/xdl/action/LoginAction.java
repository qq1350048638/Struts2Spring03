package com.xdl.action;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 登录功能
 * @author likang
 * @date   2018-1-9 下午4:37:09
 */
@Controller
@Scope("prototype")
public class LoginAction {

	private String username;//接收请求参数用户名
	private String password;//接收请求参数密码
	
	private String msg;//将错误信息，传输到前端页面
	
	public String execute(){
		
		if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
			if ("admin".equals(username) && "123123".equals(password)) {
				return "success";
			}
		}
		msg="用户名或者密码错误";
		return "error";
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
