package com.xdl.action;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * 1����������(url���ߴ��в���url)
 * 		һ������ʹ��ȫ�ֱ������գ���������getter��setter����
 * 		������ƺͲ�����ƣ����뱣��һ��
 * 2����������ķ���
 * 			public String execute(){.......}
 * 				����execute��Ӧ����struts.xml��action�е�methodֵ
 * 3������������XXXX
 * 			struts.xml----<result name="XXXX" type="">/.....</result>
 * 
 * @author likang
 * @date   2018-1-8 ����11:22:56
 */
@Controller
//id------helloAction
@Scope(value = "prototype") //取消非单例  --多例
public class HelloAction {
	
	private String name;//ȫ�ֱ���----${name }
	
	//private String _name;//��name������һ��ģ�ֻ�Ǳ�����C++�ı���ϰ��-----${_name}
						  //Ϊ�������ȫ�ֱ������Ǿֲ�����

	 public String execute(){
		 
		 /*if (name != null && !"".equals(name)) {
				JVM����Ż�
			}*/
		 //isNotEmpty��null
		 //isNotBlank��null �� ""
		if (StringUtils.isNotBlank(name)) {
		}else{
			name="struts2";
		}
		 return "success";
	 }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
