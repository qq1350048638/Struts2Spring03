package com.xdl.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xdl.dao.NoteDao;

/**
 * 删除功能操作
 * @author likang
 * @date   2018-1-9 下午2:22:02
 */
@Controller
@Scope(value="prototype")
public class DeleteAction {

	private Integer id;//接收请求中删除操作的数据ID
	
	@Resource
	private NoteDao noteDao;
	
	public String execute(){
		int count = noteDao.deleteNoteById(id);
		if (count > 0) {
			return "success";
		}
		return "error";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
