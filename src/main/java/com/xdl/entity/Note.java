package com.xdl.entity;

import java.sql.Date;

/**
 * 笔记表实体对象信息
 * @author likang
 * @date   2018-1-9 上午11:12:46
 */
public class Note {

	private Integer id; //主键ID
	private String context;//内容
	private Date publishTime;//发布时间
	private Integer likeCount;//收藏人数
	private Integer userId;//发布人ID
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public Date getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}
	public Integer getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	
	
	
	
}
