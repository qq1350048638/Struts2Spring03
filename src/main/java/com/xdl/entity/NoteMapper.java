package com.xdl.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class NoteMapper implements RowMapper{

	@Override
	public Object mapRow(ResultSet rs, int rownum) throws SQLException {
		Note note = new Note();
		note.setId(rs.getInt("id"));
		note.setContext(rs.getString("context"));
		note.setLikeCount(rs.getInt("likeCount"));
		note.setPublishTime(rs.getDate("publishTime"));
		note.setUserId(rs.getInt("userId"));
		return note;
	}
}
