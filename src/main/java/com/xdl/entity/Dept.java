package com.xdl.entity;

/**
 * 部门信息实体对象
 * @author likang
 * @date   2018-1-8 下午2:34:23
 */
public class Dept {
	
	private Long id;
	private String deptName;
	private String deptNote;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptNote() {
		return deptNote;
	}
	public void setDeptNote(String deptNote) {
		this.deptNote = deptNote;
	}
	
	
	

}
