package com.xdl.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.xdl.dao.NoteDao;
import com.xdl.entity.Note;
import com.xdl.entity.NoteMapper;


@Repository
public class NoteDaoImpl implements NoteDao{
	
//	@Autowired  //按类型匹配 一般都用autowired 方便解耦
	@Resource  // 按名称   没有 按类型匹配
	private JdbcTemplate template;
	
	@Override
	public List<Note> queryNoteByuserId(Integer userId) {
		String sql = "select id,context,publishTime,likeCount,userId from note where userID=?";
		Object[] params = {userId};
		List<Note> list = template.query(sql, params, new NoteMapper());
		return list;
	}

	@Override
	public int deleteNoteById(Integer id) {
		String sql = "delete from note where id=?";
		return template.update(sql, id);
	}

	 
}
