package com.xdl.dao;

import java.util.List;

import com.xdl.entity.Note;

/**
 * 笔记表接口
 * @author likang
 * @date   2018-1-9 上午11:10:27
 */
public interface NoteDao {
	
	/**
	 * 根据用户id，查询用户的笔记列表
	 * @param userId
	 * @return
	 */
	public List<Note> queryNoteByuserId(Integer userId);
	
	/**
	 * 根据主键ID，删除笔记信息
	 * @param id 主键ID
	 * @return
	 */
	public int deleteNoteById(Integer id);
	

}
